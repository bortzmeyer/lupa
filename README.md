Lupa, a Gemini crawler
=================================================

Lupa is a [Gemini](https://gemini.circumlunar.space/)
crawler. Starting from a few given URLs, it retrieves them, analyzes
the links in gemtext (Gemini format) files and adds them to the
database of URLs to crawl. It is not a search engine, it does not
store the content of the resources, just the metadata, for research
and statistics.

Lupa is written in [Python](https://www.python.org/).

For installation, you have to install from sources: copy the files
where you want and set the environment variable `PYTHONPATH` to this
directory.

(On a Debian machine, the prerequitises are packages python3-pip,
python3-psycopg2, python3-openssl, the Gemini library
[agunua](https://framagit.org/bortzmeyer/agunua), and two unpackaged
PyPi software, py-scfg and public_suffix_list. On other systems, pip3 will
install the dependencies. You can also install them with `pip3 install
pyopenssl psycopg2 agunua py-scfg public_suffix_list`.

Usage requires a [PostgreSQL](https://www.postgresql.org/) database,
to store the URLs and the result of crawling. Once you've created the
database, prepare it with the `create.sql` file:

```
createdb lupa
psql -f ./admin-scripts/create.sql lupa
export PYTHONPATH=$(pwd)
./admin-scripts/lupa-insert-url gemini://start.url.example/
./admin-scripts/lupa-insert-url gemini://second-start.url.example/
```

You configure Lupa from a configuration file which is, by default,
searched in `${HOME}/lupa.conf` then in `/etc/lupa.conf`. The
configuration file uses the [scfg
format](https://git.sr.ht/~emersion/scfg). An example is given in
`sample-lupa.conf`.

At the present time, you need a separate script to retrieve robots.txt
exclusion files. It is *not* done by the crawler. This script must be
run from time to time, for instance from cron, every two hours:

```
./admin-scripts/lupa-add-robotstxt
```

You run the crawler with `./lupa-crawler`. The crawler does not run
forever, you need to start it from cron. Locking is done by the
database, so it is not an issue if two instances run at the same time.

You can have a list of options with `--help` but, at this time, you
need to read the source to understand them. Some interesting options:

* `--config`: uses a different configuration file. *Warning*: the
  order of options is important, everything used before `--config`
  will be reset to the values in the configuration file. Everything
  used after overrides the configuration file.
* `--num`: maximum number of URLs to test. It is very low by default,
  to allow testing, so you may want to set it to a more reasonable
  value such as 1000.
* `--among`: number of URLs among which the "num" before are choosen
  at random. You typically set it to the size of the database, but it
  can be smaller.
* `--sleep`: by default, the crawler goes as fast as possible but you
  can slow it down with this parameter. Between two URLs, the crawler
  will sleep at a time randomly choosen between 0 and this number of
  seconds.
* `--old`: the crawler retrieves the URLs that has never been
  retrieved or retrieved more than this number of days. Default is 14 days.
* `--maximum`: the crawler has a maximum time of running, to be sure
  it is not blocked forever if there is a blocking operation. It is
  one hour by default.
* `--debug`: makes the log more talkative.

A log file is created in `/var/tmp/Lupa.log`. It is up to you to
ensure it is replaced from time to time.

Name
----

Lupa means she-wolf in latin. It refers to the wolf who took care of
the twins Romulus and Remus. (Many Gemini programs have names related
to twins, gemini in latin.)

Reference site
--------------

[On Gemini](gemini://gemini.bortzmeyer.org/software/lupa/)

On the Web, [at FramaGit](https://framagit.org/bortzmeyer/lupa>)


Author
------

Stéphane Bortzmeyer stephane+gemini@bortzmeyer.org
