#!/usr/bin/env python3

"""Clean the Lupa database by removing URLs and capsules which were
not successfully contacted for a long time."""

import getopt
import sys
import logging

import lupa.database
import lupa.config

dry_run = False
verbose = False

def usage(msg=None):
    print("Usage: %s [-n] [-v] [-o N]" % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)
        
config = lupa.config.Config()
loglevel = logging.INFO
try:
    optlist, args = getopt.getopt (sys.argv[1:], "no:hdv",
                               ["no-action", "old=", "help", "verbose"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--debug" or option == "-d":
            loglevel = logging.DEBUG
        elif option == "--no-action" or option == "-n":
            dry_run = True
        elif option == "--old" or option == "-o":
            config.purge["old"] = int(value)
        elif option == "--verbose" or option == "-v":
            verbose = True
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) != 0:
    usage()
    sys.exit(1)
    
db = lupa.database.DataBase(config, comment="Purge", logging_level=loglevel)
db.curs.execute("""SELECT id, name FROM Capsules
                       WHERE (added <= (now() - interval '%s days') AND 
                                        lastsuccessfulconnect IS NULL) OR 
                              (lastsuccessfulconnect IS NOT NULL AND 
                                        lastsuccessfulconnect <= (now() - interval '%s days'))""",
                          (config.purge["old"], config.purge["old"]))
num_capsules = db.curs.rowcount
if verbose and num_capsules > 0:
    print("""Capsules:

""")
for capsule_tuple in db.curs.fetchall():
    capsule_ref = capsule_tuple[0]
    # TODO replace by a call to delete_capsule?
    if not dry_run:
        db.curs.execute("""SELECT url FROM Urls WHERE capsule = %s""",
                (capsule_ref, ))
        for u in db.curs.fetchall():
            db.remove_url(u[0], commit=False)
        db.curs.execute("DELETE FROM Capsules_Addresses WHERE capsule = %s",
                    (capsule_ref, ))
        db.curs.execute("DELETE FROM Capsules WHERE id = %s",
                    (capsule_ref, ))
        if verbose:
            print("%s deleted" % capsule_tuple[1])
    else:
        print("%s would have been deleted" % capsule_tuple[1])
if verbose and num_capsules > 0:
    if not dry_run:
        print("%i capsules deleted" % num_capsules)
    else:
        print("%i capsules would have been deleted" % num_capsules)

# Denylisting
db.curs.execute("""UPDATE Urls SET denylisted = NULL WHERE denylisted IS NOT NULL AND denylisted < (now() - interval '%s days')""",
                (db.config.crawler["deny-old"], )) 
db.curs.execute("""UPDATE Capsules SET denylisted = NULL WHERE denylisted IS NOT NULL AND denylisted < (now() - interval '%s days')""",
                (db.config.crawler["deny-old"], )) 

# Delete old URLs
db.curs.execute("""SELECT DISTINCT Urls.id, url, laststatus FROM Urls,Capsules WHERE 
  -- Joint 
  (Urls.capsule = Capsules.Id) AND
  -- Added some time ago but never tested
  (Urls.added <= (now() - interval '%s days') AND Urls.lasttest IS NULL) OR 
  -- Tested 
  (Urls.lasttest IS NOT NULL AND 
             -- No change in status for some time                     
             (laststatuschange IS NULL OR laststatuschange <= (now() - interval '%s days')) AND (
                   -- Status indicate a permanent error, or a permanent redirect (see #26) or an unknown error
                   (substr(laststatus,1,1) = '5' OR laststatus = '31' OR laststatus !~ '^[0-9]{2}') OR (
                   -- Not tested for long, 
                   Urls.lasttest  <= (now() - interval '%s days') AND (
                        deniedbyrobotstxt = true OR
                        -- Rejected by denylisting
                        (Urls.denylisted IS NOT NULL OR Capsules.denylisted IS NOT NULL)
                        )
                   ) 
             )
   ) 
""",
                          (config.purge["old"], config.purge["old"], config.purge["old"]))
num_urls = db.curs.rowcount
if verbose and num_urls > 0:
    print("""


URLs:

""")
for url_tuple in db.curs.fetchall():
    url_ref = url_tuple[0]
    url = url_tuple[1]
    status = url_tuple[2]
    if not dry_run:
        r = db.remove_url(url)
        if verbose and r:
            print("%s deleted (status was %s)" % (url, status))
        elif not r:
            print("Warning: URL \"%s\" could not be deleted" % url, file=sys.stderr)
    else:
        if verbose:
            print("%s would have been deleted (status was %s)" % (url, status))
if verbose and num_urls > 0:
    if not dry_run:
        print("%i URLs deleted" % num_urls)
    else:
        print("%i URLs would have been deleted" % num_urls)

db.curs.execute("""SELECT id, address FROM Addresses
                       WHERE (added <= (now() - interval '%s days') AND 
                              id NOT IN (SELECT address FROM Capsules_Addresses WHERE 
                                        lastseen >= (now() - interval '%s days')))""",
                          (config.purge["old"], config.purge["old"]))
num_addresses = db.curs.rowcount
if verbose and num_addresses > 0: 
    print("""

Addresses:

""")
for address_tuple in db.curs.fetchall():
    address_ref = address_tuple[0]
    if not dry_run:
        db.curs.execute("DELETE FROM Capsules_Addresses WHERE address = %s",
                    (address_ref, ))
        db.curs.execute("DELETE FROM Addresses WHERE id = %s",
                    (address_ref, ))
        if verbose:
            print("%s deleted" % address_tuple[1])
    else:
        print("%s would have been deleted" % address_tuple[1])
if verbose and num_addresses > 0:
    if not dry_run:
        print("%i addresses deleted" % num_addresses)
    else:
        print("%i addresses would have been deleted" % num_addresses)

db.curs.execute("""SELECT id, name FROM RegisteredDomains WHERE
      added <= (now() - interval '%s days') AND 
      id NOT IN (SELECT registereddomain FROM Capsules)
      """,
                (config.purge["old"], ))
num_reg_domains = db.curs.rowcount
if verbose and num_reg_domains > 0:
    print("""

Registered domains:

""")
for domain_tuple in db.curs.fetchall():
    domain_ref = domain_tuple[0]
    if not dry_run:
        db.curs.execute("DELETE FROM RegisteredDomains WHERE id = %s",
                    (domain_ref, ))
        if verbose:
            print("%s deleted" % domain_tuple[1])
    else:
        print("%s would have been deleted" % domain_tuple[1])
if verbose and num_reg_domains > 0:
    if not dry_run:
        print("%i registered domains deleted" % num_reg_domains)
    else:
        print("%i registered domains would have been deleted" % num_reg_domains)
        
db.curs.execute("""SELECT id, name FROM Tlds WHERE
      added <= (now() - interval '%s days') AND 
      id NOT IN (SELECT tld FROM Capsules) AND
      id NOT IN (SELECT tld FROM RegisteredDomains) 
      """,
                (config.purge["old"], ))
num_tlds = db.curs.rowcount
if verbose and num_tlds > 0:
    print("""

TLDs:

""")
for tld_tuple in db.curs.fetchall():
    tld_ref = tld_tuple[0]
    if not dry_run:
        db.curs.execute("DELETE FROM Tlds WHERE id = %s",
                    (tld_ref, ))
        if verbose:
            print("%s deleted" % tld_tuple[1])
    else:
        print("%s would have been deleted" % tld_tuple[1])
if verbose and num_tlds > 0:
    if not dry_run:
        print("%i tlds deleted" % num_tlds)
    else:
        print("%i tlds would have been deleted" % num_tlds)

if not dry_run:
    db.conn.commit()
db.close()
