#!/usr/bin/env python3

MAXDAYS = 60

import sys
import urllib.parse
import getopt

import lupa.database
from lupa.utils import interval, format_size
from lupa.config import Config

display_urls = False
verbose = False
diagnostic = False
connected = False

def usage(msg=None):
    print("Usage: %s [-u] [-v] capsule-name ..." % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

config = Config()
try:
    optlist, args = getopt.getopt (sys.argv[1:], "uo:hv",
                               ["urls", "old=", "help", "verbose"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--urls" or option == "-u":
            display_urls = True
        elif option == "--verbose" or option == "-v":
            verbose = True
        elif option == "--old" or option == "-o":
            config.stats["old"] = int(value)
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) < 1:
    usage()
    sys.exit(1)

db = lupa.database.DataBase(config, comment="Info on capsule", readonly=True)
for name in args:
    name = name.lower()
    db.curs.execute("""SELECT name,certissuer,certsubject,
                              added,lastsuccessfulconnect,id FROM Capsules
                       WHERE name = %s""", (name, ))
    result = db.curs.fetchone()
    if result is None:
       print("Capsule %s not found" % name)
    else:
       capsule_ref = result[5] 
       if result[4] is None:
           last = "never successfully connected to"
       else:
           last = "last successful connection on %s (%s ago)" % (result[4], interval(result[4]))
           connected = True
       print("Capsule %s added on %s (%s ago), %s" % (result[0], result[3], interval(result[3]), last))
       if verbose:
           if connected:
               issuer = result[1]
               subject = result[2]
               if issuer == subject:
                   cert = "self-signed"
               elif issuer == "/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X3" or issuer == "/C=US/O=Let's Encrypt/CN=R3":
                   cert = "signed by Let's Encrypt"
               else:
                   cert = "signed by an unknown CA - may be a local one"
               print(cert)
               # We list all URLs, even if they are old (we rely on the purge to get rid of them)
               db.curs.execute("SELECT num_urls(%s), num_working_urls(%s), num_bytes(%s)",
                               (name, name, name))
               result = db.curs.fetchone()
               num_urls = result[0]
               num_working_urls = result[1] 
               size = result[2]
               if size is None:
                   size = "(None)"
               else:
                   size = format_size(size)
               print("%i URLs, %i successful (return code 20). %s" % \
                     (num_urls, num_working_urls, size))
               if display_urls:
                   db.curs.execute("""SELECT url, laststatus FROM Urls WHERE capsule = %s""", (capsule_ref, ))
                   results = db.curs.fetchall()
                   for result in results:
                       print("%s (%s)" % (result[0], result[1]))
                   print("")
               # May be do the same as with Urls, retrieve even old addresses and rely on the purge?
               db.curs.execute("""SELECT Addresses.address FROM Capsules,Addresses,Capsules_Addresses 
                               WHERE Capsules.id = %s AND Capsules.id = Capsules_Addresses.capsule AND
                                     Addresses.id = Capsules_Addresses.address AND 
                                     lastseen > now() - interval '%s days'""", (capsule_ref, config.stats["old"]))
               results = db.curs.fetchall()
               print("IP address(es): ", end="")
               for result in results:
                   print("%s " % result[0], end="")
               print("")
           db.curs.execute("""SELECT lastrobotstxtattempt, lastrobotstxtsuccessfulretrieval, robotstxterror, 
                                     robotstxt, denylisted 
                   FROM Capsules
                   WHERE name = %s""", (name, ))
           result = db.curs.fetchone()
           if result[0] is None:
               print("Never attempted to get robots.txt")
           elif result[1] is None:
               print("robots.txt attempted at %s (%s ago) but nothing (\"%s\")" % (result[0], interval(result[0]),
                                                                                   result[2]))
           else:
               if result[3] is None:
                   print("No robots.txt")
               else:
                   print("robots.txt is %s" % result[3][0:200])
           if result[4] is not None:
               print("Capsule denied (last time at %s - %s ago)" % (result[4], interval(result[4])))
    print("")
db.close()
