#!/usr/bin/env python3

""" Migration script from the old robots.txt system to the new one (january 2022). """

import logging
import sys

import lupa.database
import lupa.config

config = lupa.config.Config()
loglevel = logging.DEBUG

db = lupa.database.DataBase(config, comment="Populate robots.txt", logging_level=loglevel)
capsules = []
if len(sys.argv) > 1:
    for capsule_name in sys.argv[1:]:
        db.curs.execute("""SELECT id FROM Capsules WHERE name = %s""", (capsule_name,))
        result = db.curs.fetchone()
        if result is None:
            print("Unknown capsule %s" % capsule_name)
        else:
            capsules.append([result[0], capsule_name])
else:
    db.curs.execute("""SELECT id, name FROM Capsules""")
    for capsule_tuple in db.curs.fetchall():
        capsule_ref = capsule_tuple[0]
        capsule_name = capsule_tuple[1]
        capsules.append([capsule_ref, capsule_name])
for capsule in capsules:
    capsule_ref = capsule[0]
    capsule_name = capsule[1]
    print("Populating capsule %s…" % capsule_name)
    db.update_robots(capsule_ref, only_if_true=True)
db.conn.commit()
db.close()
