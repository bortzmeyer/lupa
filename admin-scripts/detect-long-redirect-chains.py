#!/usr/bin/env python3

import sys
import urllib.parse
import getopt

import lupa.database
from lupa.utils import interval, format_size
from lupa.config import Config

def usage(msg=None):
    print("Usage: %s" % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

config = Config()
try:
    optlist, args = getopt.getopt (sys.argv[1:], "h",
                               ["help"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) > 0:
    usage()
    sys.exit(1)

db = lupa.database.DataBase(config, comment="Search of long redirect chains")
db.curs.execute("""SELECT source, destination FROM Origins 
                       WHERE category = 'redirect'""")
sources = {}
for result in db.curs.fetchall():
    sources[result[0]] = True
db.close()
# For a complete analysis, we probably need a serious graph package (the graph can be messy). See #18.
print("%i sources" % len(sources))
