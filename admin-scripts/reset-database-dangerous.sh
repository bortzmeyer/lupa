#!/bin/sh

### DELETE THE DATABASE and recreate it with a few data

dropdb lupa;
createdb lupa;
psql -f ./admin-scripts/create.sql lupa
./admin-scripts/lupa-insert-url gemini://gemini.bortzmeyer.org/
./admin-scripts/lupa-insert-url gemini://gemini.circumlunar.space/
