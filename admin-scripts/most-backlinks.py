#!/usr/bin/env python3

import sys
import getopt
import re

import lupa.database
from lupa.utils import interval, format_size, canonicalize
from lupa.config import Config

# Defaults
one_link = True
show_origins = False

def usage(msg=None):
    print("Usage: %s" % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

urls = {}
def url_of(id):
    if id in urls:
        return urls[id]
    db.curs.execute("""SELECT url, Capsules.name FROM Urls,Capsules WHERE Urls.id = %s AND Capsules.Id = Urls.capsule""", (id, ))
    r = db.curs.fetchone()
    if r is None:
        return None
    urls[id] = (r[0], r[1])
    return (r[0], r[1])

try:
    optlist, args = getopt.getopt (sys.argv[1:], "h",
                               ["help", "display-origins", "all-links-per-capsule"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--display-origins":
            show_origins = True
        elif option == "--all-links-per-capsule":
            one_link = False
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) > 0:
    usage()
    sys.exit(1)

config = Config()
denylist = {}
if config.stats["denylist-filename"] is not None and config.stats["denylist-filename"] != "":
    raw_bl = open(config.stats["denylist-filename"], 'r').read()
    rest = raw_bl
    while True:
        (l, rest, eof) = lupa.utils.readline(rest)
        if re.search("^\s*#", l):
            continue
        denylist[l] = True
        if eof:
            break
db = lupa.database.DataBase(config, comment="Searching backlinks")
db.curs.execute("""SELECT source, destination FROM Origins WHERE category = 'link'""")
backlinks = {}
for tuple in db.curs.fetchall():
    source, capsule_source = url_of(tuple[0])
    dest, capsule_dest = url_of(tuple[1])
    if capsule_source not in denylist and capsule_source != capsule_dest and \
       (not one_link or (dest not in backlinks or capsule_source not in backlinks[dest]["origins"])):
        if dest in backlinks:
            backlinks[dest]["count"] += 1
            if one_link or (show_origins and capsule_source not in backlinks[dest]["origins"]):
                backlinks[dest]["origins"].append(capsule_source)
        else:
            backlinks[dest] = {"count": 1, "origins": [capsule_source,]}
db.close()
for url in sorted(backlinks.keys(), key=lambda x: backlinks[x]["count"], reverse=True):
    if backlinks[url]["count"] < 10:
        break
    if show_origins:
        origins = " (%s)" % ",".join(backlinks[url]["origins"])
    else:
         origins = ""                        
    print("%s: %i incoming links%s" % (url, backlinks[url]["count"], origins))

