#!/usr/bin/env python3

"""Removes a capsule from the database, along with all its URLs."""

import sys
import getopt
import logging

import lupa.database
import lupa.config

def usage(msg=None):
    print("Usage: %s capsule ..." % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

loglevel = logging.INFO

try:
    optlist, args = getopt.getopt (sys.argv[1:], "hd",
                               ["help", "debug"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--debug" or option == "-d":
            loglevel = logging.DEBUG
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) == 0:
    usage()
    sys.exit(1)

config = lupa.config.Config()
db = lupa.database.DataBase(config, comment="Delete capsules", logging_level=loglevel)

for capsule in args:
    db.curs.execute("SELECT id FROM Capsules WHERE name = %s",
                (capsule, ))
    c = db.curs.fetchone()
    if c is None:
        print("Warning: capsule \"%s\" not found in the database" % capsule, file=sys.stderr)
        continue
    capsule_ref = c[0]
    db.curs.execute("""SELECT url FROM Urls WHERE capsule = %s""",
                (capsule_ref, ))
    for u in db.curs.fetchall():
        db.remove_url(u[0])
    db.curs.execute("DELETE FROM Capsules_Addresses WHERE capsule = %s",
                (capsule_ref, ))
    db.curs.execute("DELETE FROM Capsules WHERE id = %s",
                (capsule_ref, ))
    db.log.debug("Capsule %s deleted", capsule)
    
db.conn.commit()
db.close()
