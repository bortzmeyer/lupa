#!/usr/bin/env python3

"""Removes an URL from the database."""

import sys
import getopt
import logging

import lupa.database
import lupa.config
import lupa.utils

canonicalize = True

def usage(msg=None):
    print("Usage: %s url ..." % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

loglevel = logging.INFO

try:
    optlist, args = getopt.getopt (sys.argv[1:], "hd",
                               ["help", "debug", "nocanonicalize"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--debug" or option == "-d":
            loglevel = logging.DEBUG
        elif option == "--nocanonicalize":
            canonicalize = False
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) == 0:
    usage()
    sys.exit(1)

config = lupa.config.Config()
db = lupa.database.DataBase(config, comment="Delete URLs", logging_level=loglevel)

for url in args:
    if canonicalize:
        canonical_url = lupa.utils.canonicalize(url)
        if canonical_url != url:
            print("URL %s changed for %s" % (url, canonical_url))
    else:
        canonical_url = url
    result = db.remove_url(canonical_url)
    if not result:
        print("Warning: something went wrong with URL \"%s\" (not in the database?)" % canonical_url, file=sys.stderr)
        continue

db.close()
