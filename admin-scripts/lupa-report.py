#!/usr/bin/env python3

DEFAULT_DURATION = 24 # Hours

import sys
import urllib.parse
import getopt

import lupa.database
from lupa.config import Config

duration = DEFAULT_DURATION

def usage(msg=None):
    print("Usage: %s [-d N]..." % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

config = Config()
try:
    optlist, args = getopt.getopt (sys.argv[1:], "d:h",
                               ["duration=", "help"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--duration" or option == "-d":
            duration = int(value)
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) > 0:
    usage()
    sys.exit(1)

db = lupa.database.DataBase(config, comment="Report")
db.curs.execute("""SELECT count(id) FROM Capsules""")
total_capsules = int(db.curs.fetchone()[0])
db.curs.execute("""SELECT count(id) FROM Capsules
                       WHERE added >= now() - interval '%s hours'""", (duration, ))
added_capsules = int(db.curs.fetchone()[0])
db.curs.execute("""SELECT count(id) FROM Capsules
                       WHERE lasttest >= now() - interval '%s hours'""", (duration, ))
tested_capsules = int(db.curs.fetchone()[0])
print("""In the last %i hours, %i capsules were tested, %i were added (they are now %i).""" %
      (duration, tested_capsules, added_capsules, total_capsules))
db.curs.execute("""SELECT count(id) FROM Urls""")
total_urls = int(db.curs.fetchone()[0])
db.curs.execute("""SELECT count(id) FROM Urls
                       WHERE added >= now() - interval '%s hours'""", (duration, ))
added_urls = int(db.curs.fetchone()[0])
db.curs.execute("""SELECT count(id) FROM Urls
                       WHERE lasttest >= now() - interval '%s hours'""", (duration, ))
tested_urls = int(db.curs.fetchone()[0])
print("""
%i URLs were tested, %i were added (they are now %i).""" %
      (tested_urls, added_urls, total_urls))
db.close()
print("""
gemini://gemini.bortzmeyer.org/software/lupa/stats.gmi
""")
