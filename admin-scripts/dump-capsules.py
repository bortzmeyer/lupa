#!/usr/bin/env python3

# See issue #6

import sys
import time

import lupa.database
import lupa.config

if len(sys.argv) != 1:
    raise Exception("Usage: %s" % sys.argv[0])

config = lupa.config.Config()
db = lupa.database.DataBase(config, comment="Dump the list of capsules")
outtxtfile = open(config.dump["capsules"] + ".txt", 'w')
outgmifile = open(config.dump["capsules"] + ".gmi", 'w')
noclosefile = open(config.dump["capsules"] + "-no-tls-close.txt", 'w')
db.curs.execute("""CREATE VIEW Recent_Capsules AS SELECT * FROM Capsules 
                           WHERE lastsuccessfulconnect IS NOT null AND 
                                 lastsuccessfulconnect >= (now() - interval '%s days')""",
                (config.stats["old"], ))
outgmifile.write("""# Capsules in Lupa database

Dump done on %s. We show only capsules with a recent (less than %s days) successful connection. 

""" % (time.strftime("%Y-%m-%d %H:%M:%SZ", time.gmtime(time.time())), config.stats["old"]))
db.curs.execute("SELECT name FROM Recent_Capsules")
capsules = []
for tuple in db.curs.fetchall():
    capsules.append(tuple[0])
for capsule in sorted(capsules):                    
    outtxtfile.write("%s\n" % capsule)
    outgmifile.write("=> gemini://%s/ %s\n" % (capsule, capsule))
outtxtfile.close()
outgmifile.close()
db.curs.execute("""CREATE VIEW Recent_NoClose_Capsules AS SELECT * FROM Capsules 
                           WHERE lastmissingtlsclose IS NOT null AND 
                                 lastmissingtlsclose >= (now() - interval '%s days')""",
                (config.stats["old"], ))
capsules = []
db.curs.execute("SELECT name FROM Recent_NoClose_Capsules")
for tuple in db.curs.fetchall():
    capsules.append(tuple[0])
for capsule in sorted(capsules):                    
    noclosefile.write("%s\n" % capsule)
# We don't commit, we changed nothing
noclosefile.close()
db.close()
