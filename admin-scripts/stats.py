#!/usr/bin/env python3

import sys
import time
import re

import lupa.utils
import lupa.database
import lupa.config
import Agunua.status

def bigint(n):
    return format(n, ",")

urls = {}
def url_of(id):
    if id in urls:
        return urls[id]
    db.curs.execute("""SELECT url, Capsules.name FROM Urls,Capsules WHERE Urls.id = %s AND Capsules.Id = Urls.capsule""", (id, ))
    r = db.curs.fetchone()
    if r is None:
        return None
    urls[id] = (r[0], r[1])
    return (r[0], r[1])

if len(sys.argv) != 1:
    raise Exception("Usage: %s" % sys.argv[0])

config = lupa.config.Config()

db = lupa.database.DataBase(config, comment="Statistics", readonly=False) # False because we create views to help us
outfile = open(config.stats["file"], 'w')

print("""# Statistics on the Gemini space

This page presents some statistics on the current state of the Gemini space. It has been updated on %s.

It cannot claim to represent the entire space. The real number of URIs is certainly higher. There are several reasons why many URIs are not in the database:

* the capsule may forbid retrieval, through robots.txt,
* we do not know all the URIs and some cannot be found from the ones we know,
* Lupa has a maximum number of URIs per capsule, to save resources (currently %s).

On this page, "working" means there was a successful connection recently. "recently" means "less than %s days". "Dead" URLs and capsules are removed after %s days and no longer appear in any statistics.

""" % (time.strftime("%Y-%m-%d %H:%M:%SZ", time.gmtime(time.time())), config.database["max-urls"], config.stats["old"], config.purge["old"]), file=outfile)

db.curs.execute("SELECT count(id) FROM Urls")
num_urls = int(db.curs.fetchone()[0])

db.curs.execute("CREATE VIEW Recent_Urls AS SELECT * FROM Urls WHERE lasttest IS NOT null AND laststatus = '20' AND lasttest >= (now() - interval '%s days')", (config.stats["old"], ))
db.curs.execute("CREATE VIEW Recent_Urls_All_Status AS SELECT * FROM Urls WHERE lasttest IS NOT null AND lasttest >= (now() - interval '%s days')", (config.stats["old"], ))

db.curs.execute("SELECT count(id) FROM Recent_Urls")
num_checked_urls = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Urls_All_Status")
num_checked_urls_all_status = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastmeta LIKE 'text/gemini%'")
num_gemini_urls = int(db.curs.fetchone()[0])

print("""
Currently, our database includes %s URIs, %s of them having been checked successfully (status code 20) and recently. Among the recently accessed, %s URIs serve a Gemini content.
""" % (bigint(num_urls), bigint(num_checked_urls), bigint(num_gemini_urls)), file=outfile)

print("""
## Resources
""", file=outfile)

db.curs.execute("SELECT sum(lastbytes) FROM Recent_Urls")
total_bytes = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes < 10")
num_bytes_10 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 10 AND lastbytes < 100")
num_bytes_100 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 100 AND lastbytes < 1000")
num_bytes_1000 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 1000 AND lastbytes < 10000")
num_bytes_10000 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 10000 AND lastbytes < 100000")
num_bytes_100000 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 100000 AND lastbytes < 1000000")
num_bytes_1000000 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE lastbytes >= 1000000")
num_bytes_10000000 = int(db.curs.fetchone()[0])

print("""The average size of the resources is %s bytes.

### Quantiles
""" % bigint(total_bytes//num_checked_urls), file=outfile)

def print_quantiles(f, mediatype=None):
    """ f is an opened file (or file-like object) """
    quantiles = 10
    for i in range(quantiles):
        quantile = (i + 1) / 10;
        if mediatype is None:
            db.curs.execute("SELECT percentile_disc(%s) WITHIN GROUP (ORDER BY lastbytes) FROM Recent_Urls" % (quantile,))
        else:
            db.curs.execute("SELECT percentile_disc(%s) WITHIN GROUP (ORDER BY lastbytes) FROM Recent_Urls WHERE mediatype='%s'" % (quantile, mediatype))
        bytes = int(db.curs.fetchone()[0])
        if i == quantiles - 1:
            msg = "."
        else:
            msg = ","
        if quantile == 0.5:
            msg += " MEDIAN"
        print("* %i%% of the resources are %s bytes or less%s" % (quantile * 100, bigint(bytes), msg),
              file=f)

print_quantiles(outfile)

print("""
#### Quantiles only for Gemini pages
""", file=outfile)
print_quantiles(outfile, "text/gemini")

print("""
### Ranges

* Less than 10 bytes: %i URLs (%.2f %%)
* 10 to 100 bytes: %i URLs (%.1f %%)
* 100 to 1000 bytes: %i URLs (%.1f %%)
* 1 to 10 kbytes: %i URLs (%.1f %%)
* 10 to 100 kbytes: %i URLs (%.1f %%)
* 100 to 1000 kbytes: %i URLs (%.1f %%)
* More than 1000 kbytes: %i URLs (%.2f %%)

""" % (num_bytes_10, 100*num_bytes_10/num_checked_urls,
       num_bytes_100, 100*num_bytes_100/num_checked_urls,
       num_bytes_1000, 100*num_bytes_1000/num_checked_urls,
       num_bytes_10000, 100*num_bytes_10000/num_checked_urls,
       num_bytes_100000, 100*num_bytes_100000/num_checked_urls,
       num_bytes_1000000, 100*num_bytes_1000000/num_checked_urls,
       num_bytes_10000000, 100*num_bytes_10000000/num_checked_urls),
      file=outfile)

db.curs.execute("""SELECT mediatype, count(id) as count FROM Recent_Urls GROUP BY mediatype ORDER BY count DESC LIMIT %s""",
                (config.stats["max-types"],))
print("""
### Most common media (MIME) types
""", file=outfile)
for mtype in db.curs.fetchall():
    print("* %s: %s URLs" % (mtype[0], bigint(mtype[1])), file=outfile)

db.curs.execute("""SELECT lang, count(id) as count FROM Recent_Urls GROUP BY lang ORDER BY count DESC LIMIT %s""",
                (config.stats["max-types"],))
print("""
### Most common languages

""", file=outfile)
for lang in db.curs.fetchall():
    if lang[0] is None or lang[0] == "":
        lang_id = "Unspecified"
    else:
        lang_id = lang[0]
    print("* %s: %s URLs" % (lang_id, bigint(lang[1])), file=outfile)
    
db.curs.execute("""SELECT langfull, count(id) as count FROM Recent_Urls GROUP BY langfull ORDER BY count DESC LIMIT %s""",
                (config.stats["max-types"],))
print("""
### Most common language tags

""", file=outfile)
for langtag in db.curs.fetchall():
    if langtag[0] is None or langtag[0] == "":
        langtag_id = "Unspecified"
    else:
        langtag_id = langtag[0]
    print("* %s: %s URLs" % (langtag_id, bigint(langtag[1])), file=outfile)
    
db.curs.execute("""SELECT charset, count(id) as count FROM Recent_Urls GROUP BY charset ORDER BY count DESC LIMIT %s""",
                (config.stats["max-types"],))
print("""
### Most common encodings ("charsets") for all files

(Remember there exists testing capsules, with very exotic encodings, so don't be surprised by some strange ones.)

""", file=outfile)
for charset in db.curs.fetchall():
    if charset[0] is None or charset[0] == "":
        charset_id = "Unspecified"
    else:
        charset_id = charset[0]
    print("* %s: %s URLs" % (charset_id, bigint(charset[1])), file=outfile)
    
db.curs.execute("""SELECT charset, count(id) as count FROM Recent_Urls WHERE mediatype = 'text/gemini' GROUP BY charset ORDER BY count DESC LIMIT %s""",
                (config.stats["max-types"],))
print("""
### Most common encodings for gemtext files only

""", file=outfile)
for charset in db.curs.fetchall():
    if charset[0] is None or charset[0] == "":
        charset_id = "Unspecified"
    else:
        charset_id = charset[0]
    print("* %s: %s URLs" % (charset_id, bigint(charset[1])), file=outfile)

db.curs.execute("""SELECT count(id) FROM Recent_Urls_All_Status WHERE laststatus LIKE 'Wrong or unknown encoding%'""")
total_wrong_encoding = int(db.curs.fetchone()[0])
print("""
By the way, %s of recently tested URLs (%.3f %%) have a wrong encoding (it does not match the actual content).
""" % (bigint(total_wrong_encoding), (100*total_wrong_encoding/num_checked_urls_all_status)), file=outfile)

print("""
### Status codes

(Remember there are test capsules with funny status codes, to exercice Gemini clients.)

""", file=outfile)

db.curs.execute("""SELECT count(id) FROM Recent_Urls_All_Status 
                      WHERE laststatus ~ '^[0-9]{2}'""")
total_status = int(db.curs.fetchone()[0])
db.curs.execute("""SELECT count(id) AS count, laststatus FROM Recent_Urls_All_Status 
                      WHERE laststatus ~ '^[0-9]{2}' 
                      GROUP BY laststatus
                      ORDER BY count DESC
                      LIMIT %s""", (config.stats["max-status"], ))
for result in db.curs.fetchall():
    try:
        explanation = Agunua.status.codes[result[1]]
    except KeyError:
        explanation = "Unknown status code, not registered"
    status_text = "%s (%s)" % (result[1], explanation)
    print("* %s: %s occurrences (%.2f %%)" % (status_text, bigint(result[0]), int(result[0])*100/total_status), file=outfile)

print("""
## Links
""", file=outfile)

denylist = {}
if config.stats["denylist-filename"] is not None and config.stats["denylist-filename"] != "":
    raw_bl = open(config.stats["denylist-filename"], 'r').read()
    rest = raw_bl
    while True:
        (l, rest, eof) = lupa.utils.readline(rest)
        if re.search("^\s*#", l):
            continue
        denylist[l] = True
        if eof:
            break
db.curs.execute("""SELECT source, destination FROM Origins WHERE category = 'link'""")
backlinks = {}
for tuple in db.curs.fetchall():
    url = url_of(tuple[0])
    if url is None:
        print("Warning: got no source for %s." % (tuple[0]), file=sys.stderr)
        continue
    source, capsule_source = url
    # In some cases, we get NULL destination, which should not happen. Insufficient transaction isolation?
    if tuple[1] is None:
        print("Warning: got Origin where source is %s/%s and destination is none." % (tuple[0], source), file=sys.stderr)
        continue
    if url_of(tuple[1]) is None:
        print("Warning: got Origin where source is %s/%s and destination is %s which was not found." % \
              (tuple[0], source, tuple[1]), file=sys.stderr)
        continue
    dest, capsule_dest = url_of(tuple[1])
    if capsule_source not in denylist and capsule_source != capsule_dest and \
       (dest not in backlinks or capsule_source not in backlinks[dest]["origins"]):
        if dest in backlinks:
            backlinks[dest]["count"] += 1
            if capsule_source not in backlinks[dest]["origins"]:
                backlinks[dest]["origins"].append(capsule_source)
        else:
            backlinks[dest] = {"count": 1, "origins": [capsule_source,]}
sum_links = 0
for link in backlinks:
    sum_links += backlinks[link]["count"]
sorted_urls = sorted(backlinks.keys(), key=lambda x: backlinks[x]["count"], reverse=True)
print("""
(We count only backlinks from external capsules, and at most one link per capsule. Also, we exclude links from capsules like search engines or directories.)

Maximum number of incoming links: %i

Average number of incoming links: %.2f
""" % (backlinks[sorted_urls[0]]["count"], sum_links/num_checked_urls_all_status), file=outfile)
      
print("""
## Capsules
""", file=outfile)

db.curs.execute("SELECT count(id) FROM Capsules")
num_capsules = int(db.curs.fetchone()[0])

db.curs.execute("CREATE VIEW Recent_Capsules AS SELECT * FROM Capsules WHERE lastsuccessfulconnect IS NOT null AND lastsuccessfulconnect >= (now() - interval '%s days')", (config.stats["old"], ))

db.curs.execute("SELECT count(id) FROM Recent_Capsules")
num_connected_capsules = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Capsules WHERE certissuer = %s OR certissuer = %s",
                ("/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X3", "/C=US/O=Let's Encrypt/CN=R3"))
num_lets_encrypt = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Capsules WHERE certissuer = certsubject");
num_auto_signed = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Capsules WHERE port != %s",
                (lupa.utils.GEMINI_PORT, ))
num_alt_ports = int(db.curs.fetchone()[0])

print("There are %i capsules. We successfully connected recently to %i of them." % (num_capsules, num_connected_capsules), file=outfile)

db.curs.execute("SELECT name, num_working_urls(name) AS num FROM Capsules ORDER BY num DESC LIMIT %s;", (config.stats["max-capsules"], ))
print("""
### Most common capsules by number of working URLs

We have a limit of %s URLs per capsule.

""" % (config.database["max-urls"]), file=outfile)
for capsule in db.curs.fetchall():
    if capsule[1] is None:
        capsule[1] = 0
    print("* %s: %i URLs" % (capsule[0], capsule[1]), file=outfile)

db.curs.execute("SELECT name, num_bytes(name) AS num FROM Capsules ORDER BY num DESC;")
# We don't use LIMIT because the capsules with zero bytes are sorted first :-(
print("""
### Most common capsules by number of bytes in working URLs 

We have a limit of bytes per URL.
=> https://framagit.org/bortzmeyer/lupa/-/issues/35 Not properly documented yet

""", file=outfile)
i = 0
for capsule in db.curs.fetchall():
    if i > config.stats["max-capsules"]:
        break
    num = capsule[1]
    if num is not None:
        print("* %s: %.1f megabytes" % (capsule[0], num/10**6), file=outfile)
        i += 1

print("""

All working capsules:

=> lupa-capsules.txt As a text file
=> lupa-capsules.gmi As a gemtext, with links

""", file=outfile)

print("""
### Certificates

%i (%.1f %%) capsules are self-signed, %i (%.1f %%) use the Certificate Authority Let's Encrypt, %i (%.1f %%) are signed by another CA (may be not a trusted one).
""" % (num_auto_signed, (100*num_auto_signed/num_connected_capsules), num_lets_encrypt, (100*num_lets_encrypt/num_connected_capsules), num_connected_capsules-num_auto_signed-num_lets_encrypt, (100*(num_connected_capsules-num_auto_signed-num_lets_encrypt)/num_connected_capsules)), file=outfile)

db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE certexpired")
num_expired = int(db.curs.fetchone()[0])
# 2021-03-10 not all capsules have this column filled in yet
db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE certexpired IS NOT NULL")
num_has_expired = int(db.curs.fetchone()[0])
print("""
%i capsules (%.2f %%) have an expired certificate.
""" % (num_expired, num_expired*100/num_has_expired), file=outfile)

db.curs.execute("SELECT certalgorithm, count(id) AS count FROM Capsules WHERE certalgorithm IS NOT NULL GROUP by certalgorithm ORDER BY count DESC")

print("""
Algorithms:
""", file=outfile)
for algo in db.curs.fetchall():
    print("* %s: %i capsules" % (algo[0], algo[1]), file=outfile)
      
db.curs.execute("SELECT certkeytype, count(id) AS count FROM Capsules WHERE certkeytype IS NOT NULL GROUP by certkeytype ORDER BY count DESC")

print("""
Key types:
""", file=outfile)
for keytype in db.curs.fetchall():
    if keytype[0] == 6:
        # See OpenSSL source code, crypto/objects/obj_dat.h, array sn_objs
        str_keytype = "RSA"
    elif keytype[0] == 408:
        str_keytype = "ECDSA" # X9.62
    elif keytype[0] == 1087:
        str_keytype = "ED25519"
    else:
        str_keytype = str(keytype[0])
    print("* %s: %i capsules" % (str_keytype, keytype[1]), file=outfile)
      
db.curs.execute("SELECT certkeysize, count(id) AS count FROM Capsules WHERE certkeysize IS NOT NULL AND certkeytype = 6 GROUP by certkeysize ORDER BY count DESC")
print("""
Key sizes for RSA:
""", file=outfile)
for keysize in db.curs.fetchall():
    print("* %s: %i capsules" % (keysize[0], keysize[1]), file=outfile)
db.curs.execute("SELECT certkeysize, count(id) AS count FROM Capsules WHERE certkeysize IS NOT NULL AND certkeytype = 408 GROUP by certkeysize ORDER BY count DESC")
print("""
Key sizes for ECDSA:
""", file=outfile)
for keysize in db.curs.fetchall():
    print("* %s: %i capsules" % (keysize[0], keysize[1]), file=outfile)

db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE tlsversion = 'TLSv1.2'")
num_tlsv12 = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE tlsversion = 'TLSv1.3'")
num_tlsv13 = int(db.curs.fetchone()[0])
# 2021-03-10: we cannot use all capsules because TLS version recording is too recent
db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE tlsversion IS NOT NULL")
num_tls = int(db.curs.fetchone()[0])

# 2021-03-10 We must test tlsshutdown is not null since not all URL
# has this information yet
# 2021-11-28 Test removed since Agunua no longer returns no_shutdown https://framagit.org/bortzmeyer/agunua/-/issues/50
#db.curs.execute("SELECT count(id) FROM Recent_Urls_All_Status WHERE not tlsshutdown")
#no_shutdown = int(db.curs.fetchone()[0])
#db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE not tlsshutdown")
#no_shutdown_status_20 = int(db.curs.fetchone()[0])
#db.curs.execute("SELECT count(id) FROM Recent_Urls_All_Status WHERE tlsshutdown IS NOT NULL")
#has_shutdown = int(db.curs.fetchone()[0]) 
#db.curs.execute("SELECT count(id) FROM Recent_Urls WHERE tlsshutdown IS NOT NULL")
#has_shutdown_status_20 = int(db.curs.fetchone()[0]) 

db.curs.execute("SELECT count(id) FROM Recent_Capsules WHERE lastmissingtlsclose IS NOT NULL AND lastmissingtlsclose >= (now() - interval '%s days')", (config.stats["old"], ))
missing_close = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM Recent_Capsules WHERE lastmissingtlsclose IS NULL OR lastmissingtlsclose >= (now() - interval '%s days')", (config.stats["old"], ))                  
has_close = int(db.curs.fetchone()[0]) 

print("""
### TLS

%.0f %% of the capsules use TLS 1.3, %.0f %% use TLS 1.2.
""" % (num_tlsv13*100/num_tls, num_tlsv12*100/num_tls), file=outfile)

# 2021-11-23: delete temporarily because unreliable. See https://framagit.org/bortzmeyer/agunua/-/issues/50 
#print("""
#%.1f %% of URLs do NOT send a proper TLS shutdown (application
#close). Even %.1f %% of those who return status 20 are in that case.
#
#=> https://gitlab.com/gemini-specification/protocol/-/issues/2 A proposal to make this shutdown mandatory.
#
#""" % (no_shutdown*100/has_shutdown, no_shutdown_status_20*100/has_shutdown_status_20), file=outfile)

#print("""%.1f %% of capsules do NOT send a proper close.
#
#=> lupa-capsules-no-tls-close.txt The full list
#
#""" % (missing_close*100/has_close, ), file=outfile)

db.curs.execute("SELECT count(id) FROM Recent_capsules WHERE robotstxt IS NOT NULL AND robotstxt <> ''")
num_robotstxt = int(db.curs.fetchone()[0])

print("""
### robots.txt

%i (%.0f %%) the capsules have a robots.txt exclusion file.
""" % (num_robotstxt, num_robotstxt*100/num_connected_capsules), file=outfile)

print("""
### Ports

%i working capsules (%.1f %%) use an alternative port
""" % (num_alt_ports, (100*num_alt_ports/num_connected_capsules)), file=outfile)

db.curs.execute("""CREATE VIEW Recent_Addresses AS SELECT * FROM Addresses WHERE 
   (SELECT max(lastseen) FROM Capsules_Addresses WHERE 
                  Capsules_Addresses.address = Addresses.id) >= (now() - interval '%s days')""",
                (config.stats["old"], ))

db.curs.execute("SELECT count(id) FROM Recent_Addresses")
num_addresses = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Addresses WHERE family(address) = 4")
num_v4_addresses = int(db.curs.fetchone()[0])

db.curs.execute("SELECT count(id) FROM Recent_Addresses WHERE family(address) = 6")
num_v6_addresses = int(db.curs.fetchone()[0])

# Does not work since the crawler does not ask for *all* IP
# addresses. We may have only IPv6 in the database even if the capsule
# has also IPv4.
#db.curs.execute("""SELECT count(Recent_Capsules.id) FROM Recent_Capsules,Addresses,Capsules_Addresses WHERE 
#            Recent_Capsules.id = Capsules_Addresses.capsule AND
#            Capsules_Addresses.address = Addresses.id AND family(Addresses.address) = 6 AND 
#            capsule NOT IN (SELECT capsule FROM Addresses,Capsules_Addresses WHERE 
#                                       Capsules_Addresses.address = Addresses.id AND family(Addresses.address) = 4)""")
#num_only_v6 = int(db.curs.fetchone()[0])

print("""
### Addresses

%i IP addresses used. %.0f %% are IPv6.

""" % (num_addresses, 100*num_v6_addresses/num_addresses), file=outfile)

db.curs.execute("SELECT  Recent_Addresses.address,count(id) AS count FROM Recent_Addresses,Capsules_Addresses WHERE Capsules_Addresses.address = Recent_Addresses.id GROUP by Recent_Addresses.address ORDER BY count DESC LIMIT %s;", (config.stats["max-addresses"], ))
print("""
#### Addresses with most virtual hosts

""", file=outfile)
for addr in db.curs.fetchall():
    print("* %s: %i vhosts" % (addr[0], addr[1]), file=outfile)

print("""
## TLDs
""", file=outfile)

db.curs.execute("SELECT count(id) FROM Tlds")
num_tlds = int(db.curs.fetchone()[0])
db.curs.execute("SELECT count(id) FROM RegisteredDomains")
num_reg_domains = int(db.curs.fetchone()[0])

print("There are %i TLDs in the capsule's names, and %i registered domains." % (num_tlds, num_reg_domains), file=outfile)

print("""
### Most common TLDs 

""", file=outfile)

db.curs.execute("SELECT Tlds.name, count(RegisteredDomains.id) AS count FROM Tlds,RegisteredDomains WHERE RegisteredDomains.tld = Tlds.id GROUP by Tlds.name ORDER BY count DESC LIMIT %s;", (config.stats["max-tlds"], ))

print("""
#### By number of registered domains

""", file=outfile)
for domain in db.curs.fetchall():
    print("* %s: %i domains" % (domain[0], domain[1]), file=outfile)

db.curs.execute("SELECT Tlds.name, count(Capsules.id) AS count FROM Tlds,Capsules WHERE Capsules.tld = Tlds.id GROUP by Tlds.name ORDER BY count DESC LIMIT %s;", (config.stats["max-tlds"], ))
print("""

#### By number of capsules

(There's a strong bias towards TLDs which have hosting services such as flounder.online, which has many capsules in subdomains. See before the TLDs per registered domains, which are probably more useful.)

""", file=outfile)
for capsule in db.curs.fetchall():
    print("* %s: %i capsules" % (capsule[0], capsule[1]), file=outfile)

print("""

## Other statistics on the geminispace

=> gemini://geminispace.info/statistics At the search engine geminispace.info
=> gemini://tlgs.one/statistics At the search engine TLGS
=> https://gitlab.com/nervuri/gemini-stats By Nervuri (specially for certificates)

## Contact

Maintained by Stéphane Bortzmeyer (email <stephane+gemini@bortzmeyer.org>). Comments and criticisms are welcome.

=> gemini://gemini.bortzmeyer.org/software/lupa/ Home page of the crawler
=> https://framagit.org/bortzmeyer/lupa Source code of the crawler

=> gemini://gemini.bortzmeyer.org/ My capsule
""", file=outfile)

# We don't commit, we changed nothing
outfile.close()
db.close()
