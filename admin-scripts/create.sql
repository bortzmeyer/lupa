CREATE TABLE Tlds (id SERIAL UNIQUE NOT NULL,
           name TEXT UNIQUE NOT NULL,
	   added TIMESTAMP NOT NULL DEFAULT now());

CREATE TABLE RegisteredDomains (id SERIAL UNIQUE NOT NULL,
           name TEXT UNIQUE NOT NULL,
	   tld INTEGER REFERENCES Tlds(id) NOT NULL,
	   added TIMESTAMP NOT NULL DEFAULT now());
	   
CREATE TABLE Capsules (id SERIAL UNIQUE NOT NULL,
           name TEXT UNIQUE NOT NULL, -- If the port is not the standard one, the name will include the port.
	   tld INTEGER REFERENCES Tlds(id) NOT NULL,
	   registereddomain INTEGER REFERENCES RegisteredDomains(id) NOT NULL,
	   port INTEGER,
	   validcert BOOLEAN,
	   tlsversion TEXT,
	   certissuer TEXT,	
	   certsubject TEXT,
	   certalgorithm TEXT,
	   certexpired BOOLEAN,
	   certkeytype INTEGER,
	   certkeysize INTEGER,
	   robotstxt TEXT,
	   lasttest TIMESTAMP,
	   lastsuccessfulconnect TIMESTAMP,
	   lastmissingtlsclose TIMESTAMP,
	   lastrobotstxtattempt TIMESTAMP,
	   robotstxterror TEXT,
	   lastrobotstxtsuccessfulretrieval TIMESTAMP, -- "Successful"
	   -- meaning we successfully contacted the server, it could
	   -- have been a 51 (not found) status code.
	   denylisted TIMESTAMP,
	   added TIMESTAMP NOT NULL DEFAULT now());	

CREATE TABLE Urls (id SERIAL UNIQUE NOT NULL,
           url TEXT UNIQUE NOT NULL,
	   capsule INTEGER REFERENCES Capsules(id) NOT NULL,
	   laststatus TEXT,
	   lastmeta TEXT,
	   mediatype TEXT,
	   lang TEXT, -- Only the first subtag, the language
		      -- itself. If NULL, it means unknown (no
		      -- retrieval), if "", it means it was retrieved
		      -- but let unspecified
	   langfull TEXT, -- The complete language tag. If NULL, it
			  -- means unknown (no retrieval), if "", it
			  -- means it was retrieved but let
			  -- unspecified
	   charset TEXT, -- If NULL, it means unknown (no retrieval), if "", it means it was retrieved but let unspecified
	   lastbytes INTEGER,
	   -- We do not store the content hosted at the URL (too heavy and we are not a search engine).
	   laststatuschange TIMESTAMP,
           lasttest TIMESTAMP,
	   tlsshutdown BOOLEAN, -- It should be per-capsule, not per
				-- URL, but currently (2021-03-08), we
				-- suspect that it may depend, for
				-- instance, of a CGI failing or not.
	   deniedbyrobotstxt BOOLEAN DEFAULT false,
	   denylisted TIMESTAMP,
	   added TIMESTAMP NOT NULL DEFAULT now());	   

CREATE TYPE origin_category AS ENUM ('link', 'redirect', 'manual', 'automatic');
-- Where do the URLs in table Urls come from?
CREATE TABLE Origins (source INTEGER REFERENCES Urls(id), -- Can be null if the origin is a manual insertion
                      destination INTEGER REFERENCES Urls(id) NOT NULL,
		      category origin_category,
		      lastseen TIMESTAMP NOT NULL DEFAULT now(),
		      UNIQUE(source, destination, category));	
-- Note that, because of the integrity constraint, we have to remove
-- origins when the URL is removed so we lose some history about the
-- origin.

CREATE TABLE Addresses (id SERIAL UNIQUE NOT NULL,
           address INET UNIQUE NOT NULL,
	   added TIMESTAMP NOT NULL DEFAULT now());

CREATE TABLE Capsules_Addresses (capsule INTEGER REFERENCES Capsules(id) NOT NULL,
       address INTEGER REFERENCES Addresses(id) NOT NULL,
       lastseen TIMESTAMP);

-- Create more indexes? See issue #29
CREATE INDEX lasttest_idx ON Urls (lasttest DESC);
CREATE INDEX denylisted_idx ON Urls (denylisted);
-- Without indexes on Origins' members, purge takes an awful lot of time
CREATE INDEX originssourceidx on Origins(source);
CREATE INDEX originsdestinationidx on Origins(destination);
-- Not yet indexes on Capsules, they are probably not enough to make
-- it worthwhile.

-- Utility functions

CREATE FUNCTION num_urls(capsule TEXT) RETURNS BIGINT AS 'SELECT count(id) FROM Urls WHERE capsule=(SELECT id FROM Capsules WHERE name = $1)' LANGUAGE 'sql';
CREATE FUNCTION num_working_urls(capsule TEXT) RETURNS BIGINT AS 'SELECT count(id) FROM Urls WHERE capsule=(SELECT id FROM Capsules WHERE name = $1) AND laststatus = ''20'' ' LANGUAGE 'sql';
CREATE FUNCTION num_bytes(capsule TEXT) RETURNS BIGINT AS 'SELECT sum(lastbytes) FROM Urls WHERE capsule=(SELECT id FROM Capsules WHERE name = $1) AND laststatus = ''20'' ' LANGUAGE 'sql';
CREATE FUNCTION num_capsules_per_tld(tld TEXT) RETURNS BIGINT AS 'SELECT count(id) FROM Capsules WHERE tld=(SELECT id FROM Tlds WHERE name = $1)' LANGUAGE 'sql';
CREATE FUNCTION num_capsules_per_domain(domain TEXT) RETURNS BIGINT AS 'SELECT count(id) FROM Capsules WHERE registereddomain=(SELECT id FROM RegisteredDomains WHERE name = $1)' LANGUAGE 'sql';
