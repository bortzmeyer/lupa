#!/usr/bin/env python3

import sys
import urllib.parse

import lupa.database

if len(sys.argv) != 1:
    raise Exception("Usage: %s" % sys.argv[0])

db = lupa.database.DataBase()

db.curs.execute("SELECT url FROM Urls")
for r in db.curs.fetchall():
    url = r[0]
    components = urllib.parse.urlparse(url)
    if components.scheme != 'gemini':
        print("Ignoring non-Gemini URL \"%s\"" % url, file=sys.stderr)
        print("")
        continue
    # Check there is a URL for the "home page"
    if components.path != "" and components.path != "/":
        test_url = "gemini://%s/" % components.netloc
        db.curs.execute("SELECT id FROM Urls WHERE url = %s", (test_url, ))
        test_url_exists = db.curs.fetchone()
        if test_url_exists is None:
            print("Adding %s" % test_url)
            db.insert_url(test_url)

db.conn.commit()

db.close()
