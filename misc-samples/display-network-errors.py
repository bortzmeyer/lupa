#!/usr/bin/env python3

import sys
import urllib.parse

import lupa.database

if len(sys.argv) != 1:
    raise Exception("Usage: %s" % sys.argv[0])

db = lupa.database.DataBase()

db.curs.execute("""SELECT url, laststatus FROM urls WHERE 
        laststatus != '20' AND
        laststatus != '30' AND
        laststatus != '31' AND
        laststatus != '40' AND
        laststatus != '50' AND
        laststatus != '51' AND 
        laststatus != '53' AND
        laststatus != '60';""")

for r in db.curs.fetchall():
    url = r[0]
    status = r[1]            
    print("%s: %s" % (url, status))

db.close()
