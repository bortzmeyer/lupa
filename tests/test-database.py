#!/usr/bin/env python3

import lupa.database
import lupa.config

config = lupa.config.Config()
db = lupa.database.DataBase(config, comment="Just a test")
db.curs.execute("SELECT count(id) FROM Urls");
print("%i URLs" % db.curs.fetchone()[0])
db.close()
