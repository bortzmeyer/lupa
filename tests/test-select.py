#!/usr/bin/env python3

import sys
import getopt
import logging

import lupa.database
import lupa.config

def usage(msg=None):
    print("Usage: %s [-n N] [-a N] [-o S]" % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

config = lupa.config.Config()
try:
    optlist, args = getopt.getopt (sys.argv[1:], "n:a:o:h",
                               ["num=", "among=", "old=", "help"])
    for option, value in optlist:
        if option == "--help" or option == "-h":
            usage()
            sys.exit(0)
        elif option == "--num" or option == "-n":
            config.crawler["num"] = int(value) 
        elif option == "--among" or option == "-a":
            config.crawler["among"] = int(value) 
        elif option == "--old" or option == "-o":
            config.crawler["old"] = int(value) 
        else:
            # Should never occur, it is trapped by getopt
            usage("Unknown option %s" % option)
            sys.exit(1)
except getopt.error as reason:
    usage(reason)
    sys.exit(1)
if len(args) != 0:
    usage()
    sys.exit(1)

db = lupa.database.DataBase(config, comment="Test select", logging_level=logging.DEBUG)
urls = db.select()
print("Asked for %i URLs, got %i" % (config.crawler["num"], len(urls)))
print(urls)
db.close()
