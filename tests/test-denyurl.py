#!/usr/bin/env python3

import sys

import lupa.database
import lupa.config

if len(sys.argv) <= 1:
    raise Exception("Usage: %s url ..." % sys.argv[0])

config = lupa.config.Config()
db = lupa.database.DataBase(config, comment="Just a test")

for url in sys.argv[1:]:
    db.deny_url(url)
db.close()
