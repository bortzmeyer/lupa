#!/usr/bin/env python3

from lupa.utils import canonicalize,InvalidUrl

import pytest

def test_hostname():
    assert canonicalize("gemini://CASE.example/") == "gemini://case.example/"

def test_path():
    assert canonicalize("gemini://foobar.example/some/thing") == "gemini://foobar.example/some/thing"

def test_nopath():
    assert canonicalize("gemini://foobar.example") == "gemini://foobar.example/"

def test_query():
    assert canonicalize("gemini://foobar.example/some/thing?a=%3F1&b=2") == "gemini://foobar.example/some/thing?a=%3F1&b=2"

def test_fragment():
    assert canonicalize("gemini://foobar.example/some/thing#baz") == "gemini://foobar.example/some/thing"
    
# Issue #24
def test_nonstandard_port():
    assert canonicalize("gemini://funny.example:1966") == "gemini://funny.example:1966/"
    assert canonicalize("gemini://funny.example:1965") == "gemini://funny.example/"
    assert canonicalize("gemini://funny.example:1966/") == "gemini://funny.example:1966/"
    assert canonicalize("gemini://funny.example:1965/") == "gemini://funny.example/"

# Issue #23 and #3
def test_nopath_query():
    assert canonicalize("gemini://cadence.moe?source=1") == "gemini://cadence.moe/?source=1"

def test_ipv6():
    assert canonicalize("gemini://[2001:db8::1]") == "gemini://[2001:db8::1]/"
    assert canonicalize("gemini://[2001:db8::1]:1965") == "gemini://[2001:db8::1]/"
    assert canonicalize("gemini://[2001:db8::1]:1966") == "gemini://[2001:db8::1]:1966/"
    
def test_invalid_url():
    with pytest.raises(InvalidUrl):
        canonicalize("gemini://foo.bar:baz/")
