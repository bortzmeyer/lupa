#!/usr/bin/env python3

import psycopg2
import random
import re
import datetime
import time
import urllib.parse
import urllib.robotparser
import sys
import logging

# https://pypi.org/project/public-suffix-list/ 
import public_suffix_list

# https://framagit.org/bortzmeyer/agunua
import Agunua
import Agunua.urltinkering

import lupa.utils
from lupa.utils import GeminiException

class DataBase:

    def __init__(self, config, comment=None, logging_level=logging.INFO, log_file=None, readonly=False):
        """ config is a lupa.config.Config object """
        self.config = config
        self.conn = psycopg2.connect(self.config.database["dbinfo"])
        # Default isolation is ISOLATION_LEVEL_READ_COMMITTED. With
        # READ_COMMITTED, external additions will be seen.  With
        # SERIALIZABLE, external additions won't be seen but conflicts
        # are too common: "psycopg2.errors.SerializationFailure: could
        # not serialize access due to read/write dependencies among
        # transactions DETAIL: Reason code: Canceled on identification
        # as a pivot, during write." Same problem with
        # REPEATABLE_READ.  Issue #40
        #self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE)
        #self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        self.curs = self.conn.cursor()
        self.curs.execute("SET timezone='UTC'")
        self.readonly = readonly
        if self.readonly:
            self.curs.execute("SET TRANSACTION READ ONLY")
        self.gen = random.Random()
        self.robotsparser = {}
        if comment is None:
            self.comment = ""
        else:
            self.comment = " for %s" % comment
        self.id = str(self.gen.randint(1, 100000))
        self.too_many_urls = {}
        logging.Formatter.converter = time.gmtime
        if log_file is None:
            log_file = self.config.database["log-file"]
        logging.basicConfig(filename=log_file,
                            format="%(levelname)s:%(asctime)s: [session " + self.id + "] %(message)s",
                            datefmt="%Y-%m-%dT%H:%M:%SZ",
                            level=logging_level)
        self.log = logging.root.getChild(self.config.database["me"])
        if self.config.crawler["socks"] is not None and self.config.crawler["socks"] != "":
            match = re.search("^\[?(([\w\.]+)|(([a-fA-F0-9:]+))|([0-9\.]+))\]?:([0-9]+)$",
                              self.config.crawler["socks"])
            if not match:
                raise GeminiException("Socks must be host:port, not \"%s\"" % \
                                      self.config.crawler["socks"])
            self.socks = (match.group(1), int(match.group(6)))
        else:
            self.socks = None
        self.log.info("Opening database %s%s", self.config.database["dbinfo"], self.comment)

    def select(self):
        self.curs.execute("""SELECT DISTINCT url FROM Urls,Capsules WHERE 
                  -- Joint 
                  Urls.capsule = Capsules.id AND 
                  -- Not tested for a long time
                  (Urls.lasttest IS NULL OR Urls.lasttest <= (now() - interval '%s days')) AND 
                  -- Capsule's robots.txt was tested recently
                  (lastrobotstxtsuccessfulretrieval IS NOT NULL AND lastrobotstxtsuccessfulretrieval >= now() - interval '%s days') AND
                  -- Authorized
                  NOT deniedbyrobotstxt AND
                  -- Not denylisted too recently
                  (Urls.denylisted IS NULL OR Urls.denylisted <= (now() - interval '%s days')) AND 
                     (Capsules.denylisted IS NULL OR Capsules.denylisted <= (now() - interval '%s days'))
                   LIMIT %s""",
                          (self.config.crawler["old"], self.config.crawler["robots-old"],
                           self.config.crawler["deny-old"], self.config.crawler["deny-old"],
                           self.config.crawler["among"]))
        urls = {}
        self.curs2 = self.conn.cursor()
        for url in self.curs.fetchall():
            urls[url[0]] = True
        total_available = len(urls)
        results = []
        for i in range(0,self.config.crawler["num"]):
            if len(urls.keys()) == 0:
                break
            intermediate = []
            for u in urls.keys():
                intermediate.append(u)
            choosen = self.gen.choice(intermediate)
            del urls[choosen]
            results.append(choosen)
        total_choosen = len(results)
        if total_available < self.config.crawler["among"]:
            pool = " (but only %i available)" % total_available
        else:
            pool = ""
        if total_choosen < self.config.crawler["num"]:
            requested = " (%i requested)" % self.config.crawler["num"]
        else:
            requested = ""
        self.log.debug("%i URLs selected%s, among a pool of %i%s",
                       total_choosen, requested,
                       self.config.crawler["among"], pool)
        return results

    def insert_url(self, url, commit=True, origin=None, category=None):
        """Inserts an URL in the database. It does *not* connect to it and
does not check it works. It is idempotent so don't hesitate to call it
without checking. "origin" indicates the URL where does it come from.

It returns a tuple of six components: 
* the URL was ignored
* the URL is already in the database
* ID of the URL
* capsule ID of the URL
* the canonicalized URL
* extra message

It records if authorized or not by robots.txt, but inserts anyway.

        """
        # Paranoia
        if category not in [None, "link", "redirect", "manual", "automatic"]:
            self.log.error("Internal error, unknown category %s when adding <%s>", category, url)
            return (True, False, None, None, None, "Unknown category")
        if (category == "manual") and origin is not None:
            self.log.error("Internal error, origin indicated when the category is manual when adding <%s>", url)
            return (True, False, None, None, None, "Origin should be None")            
        if (category == "link" or category == "redirect") and origin is None:
            self.log.error("Internal error, no origin when the category requires one when adding <%s>", url)
            return (True, False, None, None, None, "No origin")            
        if len(url) > self.config.database["max-size-url"]:
            return (True, False, None, None, None, "Too long (maximum is %i)" % self.config.database["max-size-url"])
        # See bug #17
        if "\x00" in url:
            self.log.warning("NUL character in \"%s\"", url.replace("\x00", ""))
            return (True, False, None, None, None, "NUL character present")
        url = lupa.utils.canonicalize(url)
        components = urllib.parse.urlparse(url)
        if components.scheme != 'gemini':
            # Ignoring non-Gemini URL 
            return (True, False, None, None, url, "Not a Gemini URL")
        host = components.hostname
        port = components.port
        if host is None:
            self.log.warning("No host in \"%s\"???", url)
            return (True, False, None, None, url, "No host in URL")
        if host.startswith("."): # Happened once, with .smol.pub
            self.log.warning("Invalid host in \"%s\"???", url)
            return (True, False, None, None, url, "Invalid host in URL")        
        capsule = components.netloc
        # We reject numeric hosts (literal IP addresses). We could
        # accept them (they are legal) but they are rare, and create
        # trouble 1) IPv6 addresses require brackets to separate the
        # address from the port 2) they have no TLD but the column tld
        # is mandatory. It is simpler to just reject them. See bug #13.
        match = re.search("^[0-9a-f\:]+?$", host, re.IGNORECASE)
        match2 = re.search(":", host, re.IGNORECASE)
        if match and match2:
            self.log.warning("Literal IPv6 address in URL <%s> rejected", url)
            return (True, False, None, None, url, "Literal IP address refused") 
        match = re.search("^[0-9\.]+$", host)
        match2 = re.search("\.", host)
        if match and match2:
            self.log.warning("Literal IPv4 address in URL <%s> rejected", url)
            return (True, False, None, None, url, "Literal IP address refused") 
        labels = host.split(".")
        tld = labels[len(labels)-1]
        reg_domain = public_suffix_list.registered_domain_name(host)
        # Special domain names are excluded. See also issue #27. The
        # list is
        # <https://www.iana.org/assignments/special-use-domain-names/special-use-domain-names.xml#special-use-domain>
        # but we must not blindly use the list: RFC 6761 says (section
        # 6.5) that the TLD "example" must not be treated specially
        # (Lupa is an application) but we still exclude it to avoid
        # "polluting" the database.
        if host.endswith("localhost") or host.endswith("example") or host.endswith("example.com") or host.endswith("example.net") or host.endswith("example.org") or host.endswith("invalid") or host.endswith("test") or host.endswith("i2p"):
            self.log.warning("Excluded domain suffix in %s", url)
            return (True, False, None, None, url, "Excluded domain name suffix")
        # Check there is a URL for the "home page"
        added_root = False
        if components.path != "" and components.path != "/":
            test_url = "gemini://%s/" % (capsule, )
            self.curs.execute("SELECT id FROM Urls WHERE url = %s", (test_url, ))
            test_url_exists = self.curs.fetchone()
            if test_url_exists is None:
                (ignored, already_there, id, capsule_id, canonical_url, msg) = self.insert_url(test_url, commit=False)
                if already_there:
                    raise GeminiException("URL %s was not supposed to be alreday there" % test_url)
                if not ignored:
                    added_root = True
                    root_url_ref = id
        self.curs.execute("SELECT id, capsule FROM Urls WHERE url = %s", (url, ))
        existing_url = self.curs.fetchone()
        if existing_url is not None:
            self.update_origins(existing_url[0], category, origin)
            if commit:
                self.conn.commit()
            return(False, True, existing_url[0], existing_url[1], url, None)
        # num_urls is slow (see issue #29). We optimize by memoizing the results:
        if capsule in self.too_many_urls:
            return (True, False, None, None, url, "Too many URL, maximum is %i, already measured" % self.config.database["max-urls"])
        self.curs.execute("SELECT num_urls(%s)", (capsule,))
        n = self.curs.fetchone()
        if n is not None and n[0] >= self.config.database["max-urls"]:
            self.too_many_urls[capsule] = True # Will be kept for the entire run. Crawler runs are short, anyway.
            self.log.debug("%s not added because %s already has more than %i URLs", url, capsule,
                           self.config.database["max-urls"])
            return (True, False, None, None, url, "Too many URL, maximum is %i" % self.config.database["max-urls"])
        self.curs.execute("SELECT id FROM Tlds WHERE name = %s", (tld, ))
        tld_ref = self.curs.fetchone()
        if tld_ref is None:
            self.curs.execute("INSERT INTO Tlds (name) VALUES (%s)", (tld, ))
            self.curs.execute("SELECT id FROM Tlds WHERE name = %s", (tld, ))
            tld_ref = self.curs.fetchone()
        self.curs.execute("SELECT id FROM RegisteredDomains WHERE name = %s", (reg_domain, ))
        reg_domain_ref = self.curs.fetchone()
        if reg_domain_ref is None:
            self.curs.execute("INSERT INTO RegisteredDomains (name, tld) VALUES (%s, %s)", (reg_domain, tld_ref))
            self.curs.execute("SELECT id FROM RegisteredDomains WHERE name = %s", (reg_domain, ))
            reg_domain_ref = self.curs.fetchone()
        self.curs.execute("SELECT id FROM Capsules WHERE name = %s", (capsule, ))
        capsule_ref = self.curs.fetchone()
        if capsule_ref is None:
            self.curs.execute("INSERT INTO Capsules (name, tld, registereddomain, port) VALUES (%s, %s, %s, %s)",
                              (capsule, tld_ref, reg_domain_ref, port))
            self.curs.execute("SELECT id FROM Capsules WHERE name = %s", (capsule, ))
            capsule_ref = self.curs.fetchone()
        new_components = urllib.parse.urlparse(url)
        authorized = self.check_robots(capsule_ref, new_components.path)
        self.curs.execute("INSERT INTO Urls (url, capsule, deniedbyrobotstxt) VALUES (%s, %s, %s)",
                          (url, capsule_ref, not authorized))
        self.curs.execute("SELECT id FROM Urls WHERE url = %s", (url, ))
        url_ref = self.curs.fetchone()
        self.update_origins(url_ref, category, origin)
        if added_root:
            self.update_origins(root_url_ref, "automatic", url)
        if commit:
            self.conn.commit()
        self.log.info("Inserted <%s>", url)
        return (False, False, url_ref[0], capsule_ref[0], url, "OK, inserted")

    def test_url(self, url):
        """Connects to an URL and insert the result in the database. Warning:
the caller must ensure that the robots.txt of the capsule has been
retrieved. Returns a couple of values:
* a boolean indicating that something has been done.
* a string giving details"""
        if url.find("??") != -1 or url[8:].find("//") != -1: # 8 is to skip 'gemini://'
            self.log.debug("Who called test_url(\"%s\")?", url)
            return (False, "Invalid URL (?? or // or stuff like that")
        insertion = self.insert_url(url)
        if not insertion[1]:
            return (False, insertion[5])
        url_ref = insertion[2]
        capsule_ref = insertion[3]
        url = insertion[4]
        self.curs.execute("SELECT deniedbyrobotstxt FROM Urls WHERE id = %s", (url_ref, ))
        deniedbyrobotstxt = self.curs.fetchone()
        if deniedbyrobotstxt is None:
            raise GeminiException("No indication of robots.txt retrieved for %s" % url)
        if deniedbyrobotstxt[0]:
            self.log.info("Ignoring <%s> because of robots.txt", url)
            return (False, "Denied by robots.txt")
        components = urllib.parse.urlparse(url)
        if components.hostname.endswith(".onion") and self.socks is not None:
            socks = self.socks
        else:
            socks = None
        # Clearing the column lastrejectrobotstxt is done by the purge program
        self.log.debug("Trying to retrieve %s …", url)
        result = Agunua.GeminiUri(url, get_content=True,
                                  parse_content=True,
                                  binary=False,
                                  insecure=self.config.database["insecure"],
                                  ignore_missing_tls_close=True,
                                  use_socks=socks,
                                  tofu=self.config.database["tofu"],
                                  accept_expired=self.config.database["accept-expired"])
        if result.network_success and result.status_code == "20" and result.binary:
            # Actual content does not always match the declared
            # charset, hence the test. See
            # <https://framagit.org/bortzmeyer/agunua/-/issues/22>
            # and
            # <https://framagit.org/bortzmeyer/agunua/-/issues/23>
            self.log.error("<%s> has a wrong or unknown encoding: %s", url, result.error)
            result.status_code = "Wrong or unknown encoding: %s" % result.error
        # self.log.debug("<%s> returning %s", url, result) # Too talkative
        self.curs.execute("SELECT laststatus FROM Urls WHERE id = %s", (url_ref, ))
        old_status = self.curs.fetchone()[0]
        if result.network_success:
            # We set laststatus (and lastmeta) even if there is no
            # change. Is it a problem, for instance for performance?
            self.curs.execute("UPDATE Urls SET lasttest = now(), laststatus = %s, lastmeta = %s WHERE id = %s",
                              (result.status_code, result.meta, url_ref))
            # Removed on 2021-11-28, not reliable enough, see https://framagit.org/bortzmeyer/agunua/-/issues/50
            #self.curs.execute("UPDATE Urls SET lasttest = now(), laststatus = %s, lastmeta = %s, tlsshutdown = %s WHERE id = %s",
            #                  (result.status_code, result.meta, not result.no_shutdown, url_ref))
            if self.curs.rowcount != 1:
                raise GeminiException("UPDATE of URL %s affected %i rows" % (url_ref, self.curs.rowcount))
            if result.possible_truncation_issue:
                self.curs.execute("UPDATE Capsules SET lastmissingtlsclose = now() WHERE id = %s", (capsule_ref,))
            if result.status_code == "20":
                # Extract the first subtag of the language tag, the one about the language itself
                try:
                    (langshort, rest) = result.lang.split('-', maxsplit=1)
                except ValueError:
                    langshort = result.lang
                self.curs.execute("UPDATE Urls SET lastbytes = %s, mediatype = %s, lang = %s, langfull = %s, charset = %s WHERE id = %s",
                              (result.size, result.mediatype, langshort, result.lang, result.charset, url_ref))
                if self.curs.rowcount != 1:
                    raise GeminiException("UPDATE of %s affected %i rows" % (url_ref, self.curs.rowcount))
            if result.links is not None:
                for link in result.links:
                    if link.find("??") != -1:
                        # Bug in Agunua <= version 0.7, solved in the future 0.8
                        self.log.warning("Link <%s> with two question marks in <%s>", link, url)
                    elif link[8:].find("//") != -1: # 8 is to skip 'gemini://'
                        # Where is the bug? gus.guru returns that
                        self.log.warning("Link <%s> with two slashes in <%s>", link, url)
                    else:
                        try:
                            self.insert_url(link, commit=False, origin=url, category="link")
                        except lupa.utils.InvalidUrl:
                            self.log.warning("Link %s is invalid", link)
            if result.status_code == "30" or result.status_code == "31":
                # Protection against a MollyBrown bug (2021-01-17). Fixed on 2021-01-25 but no guarantee everyone will fix, and there is also the Agunua <= 0.7 bug mentioned earlier.
                if result.meta.find("??") == -1 and result.meta[8:].find("//") == -1: # 8 is to skip 'gemini://'
                    self.log.debug("Redirect from %s to %s", url, result.meta)
                    try:
                        self.insert_url(Agunua.urltinkering.urlmerge(url, result.meta), commit=False,
                                    origin=url, category="redirect")
                    except lupa.utils.InvalidUrl:
                        self.log.warning("Redirect target %s is invalid", result.meta)
                else:
                    self.log.debug("Ignoring wrong redirection from <%s> to <%s>", url, result.meta)
                    self.curs.execute("UPDATE Urls SET lasttest = now() WHERE url = %s", (url, ))
                    if self.curs.rowcount != 1:
                        raise GeminiException("UPDATE of URL %s affected %i rows" % (url_ref, self.curs.rowcount))
                self.conn.commit()
                return (False, "Redirected")

        else:
            # We set laststatus even if there is no change. Is it a
            # problem, for instance for performance?
            self.curs.execute("UPDATE Urls SET lasttest = now(), laststatus = %s WHERE id = %s", (result.error, url_ref))
            if self.curs.rowcount != 1:
                raise GeminiException("UPDATE of %s affected %i rows" % (url_ref, self.curs.rowcount))
        if (result.network_success and old_status != result.status_code) or \
           (not result.network_success and old_status != result.error):
            self.curs.execute("UPDATE Urls SET laststatuschange = now() WHERE id = %s",
                              (url_ref, ))
            if self.curs.rowcount != 1:
                raise GeminiException("UPDATE of URL %s affected %i rows" % (url_ref, self.curs.rowcount))
        self.curs.execute("UPDATE Capsules SET lasttest = now() WHERE id = %s",
                          (capsule_ref, ))
        if self.curs.rowcount != 1:
            raise GeminiException("UPDATE of capsule %s affected %i rows" % (capsule_ref, self.curs.rowcount))
        if result.network_success:
            self.curs.execute("UPDATE Capsules SET tlsversion = %s, certissuer = %s, certsubject = %s, certalgorithm = %s, certexpired = %s, certkeytype = %s, certkeysize = %s, lastsuccessfulconnect = now() WHERE id = %s",
                              (result.tls_version, result.issuer, result.subject, result.cert_algo, result.expired, result.cert_key_type, result.cert_key_size, capsule_ref, ))
            if self.curs.rowcount != 1:
                raise GeminiException("UPDATE of capsule %s affected %i rows" % (capsule_ref, self.curs.rowcount))
            if socks is None:
                try:
                    address = result.ip_address
                    if address is not None:
                        self.curs.execute("SELECT id FROM Addresses WHERE address = %s", (address, ))
                        ip = self.curs.fetchone()
                        if ip is None:
                            self.curs.execute("INSERT INTO Addresses (address) VALUES (%s)", (address, ))
                            self.curs.execute("SELECT id FROM Addresses WHERE address = %s", (address, ))
                            ip = self.curs.fetchone()
                        ip_ref = ip[0]
                        self.curs.execute("SELECT address FROM Capsules_Addresses WHERE address = %s AND capsule = %s", (ip_ref, capsule_ref))
                        ip = self.curs.fetchone()
                        if ip is None:
                            self.curs.execute("INSERT INTO Capsules_Addresses (address, capsule, lastseen) VALUES (%s, %s, now())",
                                          (ip_ref, capsule_ref))
                        else:
                            self.curs.execute("UPDATE Capsules_Addresses SET lastseen = now() WHERE address = %s and capsule = %s", (ip_ref, capsule_ref))
                            if self.curs.rowcount != 1:
                                raise GeminiException("UPDATE of capsules_addresses %s,%s affected %i rows" % (ip_ref, capsule_ref, self.curs.rowcount))
                except AttributeError:
                    pass
        self.conn.commit()
        if result.network_success:
            if result.status_code == "20":
                if result.mediatype == "text/gemini":
                    success = "OK (%i links)" % len(result.links)
                else:
                    success = "OK"
            else:
                success = "OK (status code %s)" % result.status_code
        else:
            success = "FAIL"
            self.log.debug("Network problem at %s: \"%s\"", url, result.error)
        self.log.info("Got %s and updated %s", url, success)
        return (result.network_success, result.status_code)

    def remove_url(self, url, commit=True):
        """Removes an URL from the database. Return True if removed
successfully, False if it did not exist or if something went
wrong."""
        self.curs.execute("SELECT id FROM Urls WHERE url = %s",
                (url, ))
        c = self.curs.fetchone()
        if c is None:
            self.log.warning("URL %s cannot be deleted, it is not in the database", url)
            return False
        url_ref = c[0]
        self.curs.execute("DELETE FROM Origins WHERE source = %s OR destination = %s",
                (url_ref, url_ref))
        self.curs.execute("DELETE FROM Urls WHERE id = %s",
                (url_ref, ))
        if self.curs.rowcount != 1:
            self.log.error("Cannot delete URL %s, DELETE SQL request failed", url)
            return False
        if commit:
            self.conn.commit()
        self.log.debug("URL %s deleted", url)
        return True

    def update_origins(self, url_ref, category, origin):
        if category is not None:
            if origin is not None:
                self.curs.execute("SELECT id FROM Urls WHERE url = %s", (origin, ))
                data = self.curs.fetchone()
                if data is None:
                    self.log.error("Internal error, origin %s does not exist", origin)
                    return(True, False, None, None, url_ref)
                origin_ref = data[0]
                self.curs.execute("SELECT source, destination FROM Origins WHERE source = %s AND destination = %s AND category = %s", (origin_ref, url_ref, category))
            else:
                origin_ref = None
                self.curs.execute("SELECT source, destination FROM Origins WHERE source is null AND destination = %s AND category = %s", (url_ref, category))
            if self.curs.fetchone() is None:
                self.curs.execute("INSERT INTO Origins (source, destination, category) VALUES (%s, %s, %s)",
                                  (origin_ref, url_ref, category))
            else:
                self.curs.execute("UPDATE Origins SET lastseen = now() WHERE source = %s AND destination = %s AND category = %s",
                                  (origin_ref, url_ref, category))

    def deny_capsule(self, capsule, commit=True):
        self.curs.execute("SELECT id FROM Capsules WHERE name = %s", (capsule, ))
        existing_capsule = self.curs.fetchone()
        if existing_capsule is None:
            self.log.warning("Capsule %s cannot be denied, it does not exist" % capsule) # May be it was simply purged
            return
        capsule_ref = existing_capsule[0]
        self.curs.execute("UPDATE Capsules SET denylisted = now() WHERE id = %s", (capsule_ref, ))
        if self.curs.rowcount != 1:
            raise GeminiException("UPDATE of capsule (denylisted) %s affected %i rows" % (capsule_ref, self.curs.rowcount))
        if commit:
            self.conn.commit()
        self.log.debug("Capsule %s denied" % capsule)
        
    def deny_url(self, url, commit=True):
        self.curs.execute("SELECT id FROM Urls WHERE url = %s", (url, ))
        existing_url = self.curs.fetchone()
        if existing_url is None:
            self.log.warning("URL %s cannot be denied, it does not exist" % url) # May be it was simply purged
            return
        url_ref = existing_url[0]
        self.curs.execute("UPDATE Urls SET denylisted = now() WHERE id = %s", (url_ref, ))
        if self.curs.rowcount != 1:
            raise GeminiException("UPDATE of URL (denylisted) %s affected %i rows" % (url_ref, self.curs.rowcount))
        if commit:
            self.conn.commit()
        self.log.debug("URL %s denied" % url)

    def check_robots(self, capsule, path):
        """ Checks if access to this path on this capsule is
        authorized by robots.txt. Returns a boolean (True: access
        authorized). """
        if capsule not in self.robotsparser:
            self.robotsparser[capsule] = urllib.robotparser.RobotFileParser()
        self.curs.execute("SELECT robotstxt FROM Capsules WHERE id = %s", (capsule, ))
        result = self.curs.fetchone()
        if result is None:
            return True
        robotstxt = result[0]
        if robotstxt is None or robotstxt == "":
            return True
        else:
            self.robotsparser[capsule].parse(re.split("\r\n|\r|\n",
                                                      robotstxt)) # It seems it never fails, even for broken files
            return self.robotsparser[capsule].can_fetch(lupa.utils.MY_BOT_NATURE, path)

    def update_robots(self, capsule, only_if_true = False):
        """Update the deniedbyrobotstxt status of all the URLs of this
        capsule. Don't change only_if_true if you don't know what you
        are doing.
        """
        if capsule not in self.robotsparser:
            self.robotsparser[capsule] = urllib.robotparser.RobotFileParser()
        self.curs.execute("SELECT name, robotstxt FROM Capsules WHERE id = %s", (capsule, ))
        result = self.curs.fetchone()
        if result is None:
            self.log.error("No capsule of ID %s for updating robots.txt" % capsule)
        capsule_name = result[0]
        robotstxt = result[1]        
        if robotstxt is not None and robotstxt != "":
            self.robotsparser[capsule].parse(re.split("\r\n|\r|\n", robotstxt)) # It seems it never fails, even for broken files
        self.log.debug("Updating robots.txt for capsule %s" % capsule_name)
        self.curs.execute("SELECT Urls.id, url FROM Urls, Capsules WHERE Urls.capsule = Capsules.id AND Urls.capsule = %s", (capsule, ))
        for url in self.curs.fetchall():
            url_ref = url[0]
            if robotstxt is None or robotstxt == "":
                denied = False
            else:
                components = urllib.parse.urlparse(url[1])
                denied = not self.robotsparser[capsule].can_fetch(lupa.utils.MY_BOT_NATURE, components.path)
            if only_if_true and not denied:
                pass
            else:
                self.curs.execute("UPDATE Urls SET deniedbyrobotstxt = %s WHERE id = %s", (denied, url_ref))
        self.conn.commit()
            
    def robotscapsule(self, capsule, port=lupa.utils.GEMINI_PORT):
        """Retrieve the robots.txt of the capsule and stores it. Updates all
           the URLs of this capsule. Returns a triple (capsule exists
           in database, capsule contacted, file retrieved
           successfully). gemini://gemini.circumlunar.space/docs/companion/robots.gmi

        """
        self.curs.execute("SELECT id FROM Capsules WHERE name = %s", (capsule, ))
        existing_capsule = self.curs.fetchone()
        if existing_capsule is None:
            return (False, False, False)
        capsule_ref = existing_capsule[0]
        tport = ""
        if port != lupa.utils.GEMINI_PORT:
            tport = ":%i" % port
        # The text after '?' is a trick to workaround the lack of
        # User-Agent: in Gemini. So, we announce ourselves
        # politely. Unfortunately, some servers balk on it, for
        # instance gemini://alexschroeder.ch/ returns 50 (bad request)
        # :-(
        url = "gemini://%s%s/robots.txt?robot=true&uri=%s" % (capsule, tport,
                                                              urllib.parse.quote_plus(self.config.database["url"]))
        if capsule.endswith(".onion") and self.socks is not None:
            socks = self.socks
        else:
            socks = None
        try:
            result = Agunua.GeminiUri(url, get_content=True, binary=False,
                                  follow_redirect=True, insecure=self.config.database["insecure"],
                                  tofu=self.config.database["tofu"],
                                  accept_expired=self.config.database["accept-expired"],
                                  ignore_missing_tls_close=True,
                                  use_socks=socks)
        except lupa.utils.InvalidUrl:
            self.log.warning("Cannot get robots.txt for %s, URL is invalid", url)
            self.curs.execute("UPDATE Capsules SET lastrobotstxtattempt = now(), robotstxterror = %s WHERE id = %s", ("Invalid URL", capsule_ref))
            self.conn.commit() 
            return (True, True, False)
        if result.network_success:
            new_robotstxt = False
            self.curs.execute("SELECT robotstxt FROM Capsules WHERE id = %s", (capsule_ref, ))
            tuple = self.curs.fetchone()
            if tuple is not None:
                old_robotstxt = tuple[0]
            else:
                old_robotstxt = None
            if result.status_code == "20":
                if not result.mediatype.startswith("text/"): # Some capsules wrongly serve robots.txt with type application/octet-stream which makes it returned as binary.
                    self.log.debug("Wrong media type for %s/robots.txt: %s", capsule, result.mediatype)
                    self.curs.execute("UPDATE Capsules SET lastrobotstxtattempt = now(), robotstxterror = %s WHERE id = %s", (result.mediatype, capsule_ref))
                    self.conn.commit() 
                    return (True, True, False)
                self.log.info("Got robots.txt for %s", capsule)
                if result.payload != old_robotstxt:
                    self.update_robots(capsule_ref)
                self.curs.execute("UPDATE Capsules SET lastsuccessfulconnect = now(), lastrobotstxtattempt = now(), lastrobotstxtsuccessfulretrieval = now(), robotstxt = %s WHERE id = %s",
                                  (result.payload, capsule_ref))
                if self.curs.rowcount != 1:
                    raise GeminiException("UPDATE of capsule %s affected %i rows" % (capsule_ref, self.curs.rowcount))
                self.conn.commit()
                return (True, True, True)
            else:
                self.log.debug("Got no robots.txt for %s, code %s", capsule, result.status_code)
                if old_robotstxt is not None:
                    self.update_robots(capsule_ref)                    
                self.curs.execute("UPDATE Capsules SET lastsuccessfulconnect = now(), lastrobotstxtattempt = now(), lastrobotstxtsuccessfulretrieval = now(), robotstxt = NULL, robotstxterror = NULL WHERE id = %s",
                              (capsule_ref, ))
                if self.curs.rowcount != 1:
                    raise GeminiException("UPDATE of capsule %s affected %i rows" % (capsule_ref, self.curs.rowcount))
                self.conn.commit()
                return (True, True, False)
        else:
            self.log.debug("Network problem getting robots.txt for %s: %s", capsule, result.error)
            # At least we tried. Record that to avoid too frequent requests.
            self.curs.execute("UPDATE Capsules SET lastrobotstxtattempt = now(), robotstxterror = %s, robotstxt = NULL WHERE id = %s",
                              (result.error, capsule_ref, ))
            if self.curs.rowcount != 1:
                raise GeminiException("UPDATE of capsule %s affected %i rows" % (capsule_ref, self.curs.rowcount))
            self.conn.commit()
            return (True, False, False)
    
    def close(self):
        self.log.info("Closing database %s%s", self.config.database["dbinfo"], self.comment)
        self.curs.close()
        self.conn.close()
        logging.shutdown()

def error(msg=None):
    if msg is None:
        msg = "Unknown error"
    raise GeminiException(msg)

