#!/usr/bin/env python3

VERSION = 0.1

GEMINI_PORT = 1965

MY_BOT_NATURE = "researcher" # gemini://gemini.circumlunar.space/docs/companion/robots.gmi

import datetime
import urllib.parse
import re

class GeminiException(Exception):
    pass

class InvalidUrl(GeminiException):
    pass

def readline(s):
    """ Returns a triple (line_read, rest_of_the_data, eof) """
    nextline = 0
    for char in s:
        nextline += 1
        if char == "\r" and s[nextline] == "\n":
            l = s[0:nextline-1]
            return (l, s[nextline+1:], False)
        if char == "\n":
            l = s[0:nextline-1]
            return (l, s[nextline:], False)            
    return (s, None, True)

def interval(d):
    """Returns a human-readable string about how old is the date 'd' (which
       must be a datetime.datetime object"""
    now = datetime.datetime.utcnow()
    offset = now - d
    if offset > datetime.timedelta(days=365.25):
        return "%i years" % int(offset.days/365.25)
    elif offset > datetime.timedelta(days=30.5):
        return "%i months" % int(offset.days/30.5)
    elif offset > datetime.timedelta(weeks=1):
        return "%i weeks" % int(offset.days/7)
    elif offset > datetime.timedelta(days=1):
        return "%i days" % offset.days
    elif offset > datetime.timedelta(hours=1):
        return "%i hours" % int(offset.seconds/3600)
    elif offset > datetime.timedelta(minutes=1):
        return "%i minutes" % int(offset.seconds/60)
    else:
        return "%i seconds" % offset.seconds
   
def format_size(s):
    """Returns a human-readable string of the size in bytes 's' """
    if s > 10**9:
        return "%i gigabytes" % int(s/10**9)
    elif s > 10**6:
        return "%i megabytes" % int(s/10**6)
    elif s > 10**3:
        return "%i kilobytes" % int(s/10**3)
    else:
        return "%i bytes" % s

def canonicalize(url):
    """ url must be an absolute URL """
    components = urllib.parse.urlparse(url)
    if components.scheme != 'gemini':
        # We don't play with them
        return url
    host = components.hostname
    if host is None or host == "":
        raise InvalidUrl(url)
    if host.endswith("."):
        host = host[:-1]
    match = re.search("^[0-9a-f\:]+$", host, re.IGNORECASE)
    match2 = re.search(":", host, re.IGNORECASE)
    if match and match2: # We may find host names that look like IP
                         # addresses (such as
                         # "05a038cad08346fbd30b00086e99b7073349ffe8cade0338322122cb751bea8c4c"
                         # in the links of
                         # <gemini://arkd.flounder.online/>).
        host = "[%s]" % host 
    capsule = host # But we may add a port later if a non-standard port is used
    port = GEMINI_PORT
    try: # urllib.parse, when encountering an URL like gemini://foo.bar:baz/ loads "baz" as the port but crashes at the first access
        if components.port is not None:
            port = int(components.port)
            if components.port == GEMINI_PORT:
                port = GEMINI_PORT
    except ValueError:
        raise InvalidUrl(url)
    tport = ""
    if port != GEMINI_PORT:
        tport = ":%i" % port
    capsule = "%s%s" % (capsule, tport)
    if components.path == "": 
        path = "/" # This is questionable: RFC 3986 does not say that
                   # empty paths and paths equal to "/" are the
                   # same. Nevertheless, all Gemini servers behave
                   # that way.
    else:
        # We don't canonicalize *inside* the path but may be we should. See issue #22
        path = components.path 
    return urllib.parse.urlunsplit(("gemini", capsule, path, components.query, None)) # The last
                       # None is because we ignore fragments
