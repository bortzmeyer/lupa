#!/usr/bin/env python3

# https://hg.code.netlandish.com/~petersanchez/py-scfg
# https://pypi.org/project/py-scfg/
import scfg

import os
import sys

from lupa.utils import GeminiException

class ConfigParsingException(GeminiException):
    pass

PATH="%s/lupa.conf:/etc/lupa.conf" % os.environ["HOME"]

DEFAULTS_CRAWLER = {
    "verbose": False,
    "debug": False,
    "num": 3,
    "among": 10,
    "sleep": None, 
    "old": 14,
    "maximum-time": 3600,
    "robots-old": 30,
    "robots-refresh": 20,
    "deny-old": 15,
    "denylist-filename": None,
    "url-denylist-filename": None,
    "socks": None
    }

DEFAULTS_DATABASE = {
    "me": "Lupa",
    "url": "gemini://gemini.bortzmeyer.org/software/lupa/",
    "dbinfo": "dbname=lupa",
    "log-file": "/var/log/lupa/Lupa.log",
    "max-urls": 100,
    "max-size-url": 1024,
    "insecure": True,
    "tofu": "",
    "accept-expired": True
    }

DEFAULTS_PURGE = {
    "old": 60
    }

DEFAULTS_STATS = {
    "file": "stats.gmi",
    "max-addresses": 12, 
    "max-types": 20,
    "max-tlds": 20,
    "max-capsules": 20,
    "max-status": 12,
    "old": 31,
    "denylist-filename": None
    }

DEFAULTS_DUMP = {
    "capsules": "lupa-capsules" # Do not add an extension, it will be done in the dump program
    }

class Config():
    
    def __init__(self, path=PATH, must_exist=False):
        conffiles = path.split(":")
        found = False
        for conffile in conffiles:
            self.conf = scfg.Config(conffile)
            try:
                self.conf.load()
                self.filename = conffile
                found = True
                break
            except FileNotFoundError:
                continue
            except ValueError as e:
                raise ConfigParsingException("Cannot parse configuration file %s: %s" % (conffile, e))
        if not found:
            if must_exist:
                raise ConfigParsingException("Configuration file %s not found" % conffile)
            self.crawler = DEFAULTS_CRAWLER
            self.database = DEFAULTS_DATABASE
            self.purge = DEFAULTS_PURGE
            self.stats = DEFAULTS_STATS
            self.dump = DEFAULTS_DUMP
        else:
            self.crawler = {}
            self.database = {}
            self.purge = {}
            self.stats = {}
            self.dump = {}
            self.set("crawler", "verbose", is_boolean=True)
            self.set("crawler", "debug", is_boolean=True)
            self.set("crawler", "maximum-time", is_integer=True)
            self.set("crawler", "num", is_integer=True)
            self.set("crawler", "among", is_integer=True)
            self.set("crawler", "sleep", is_integer=True)
            self.set("crawler", "old", is_integer=True)
            self.set("crawler", "robots-old", is_integer=True)
            self.set("crawler", "robots-refresh", is_integer=True)
            self.set("crawler", "deny-old", is_integer=True)
            self.set("crawler", "denylist-filename")
            self.set("crawler", "url-denylist-filename")
            self.set("crawler", "socks")
            self.set("database", "max-urls", is_integer=True)
            self.set("database", "max-size-url", is_integer=True)
            self.set("database", "me")
            self.set("database", "dbinfo")
            self.set("database", "url")
            self.set("database", "log-file")
            self.set("database", "insecure", is_boolean=True)
            self.set("database", "tofu")
            self.set("database", "accept-expired", is_boolean=True)
            self.set("purge", "old", is_integer=True)
            self.set("stats", "file")
            self.set("stats", "max-addresses", is_integer=True)
            self.set("stats", "max-types", is_integer=True)
            self.set("stats", "max-tlds", is_integer=True)
            self.set("stats", "max-capsules", is_integer=True)
            self.set("stats", "max-status", is_integer=True)
            self.set("stats", "old", is_integer=True)
            self.set("stats", "denylist-filename")
            self.set("dump", "capsules")

    def set(self, category, param, is_integer=False, is_boolean=False):
        if is_integer and is_boolean:
            raise GeminiException("Internal error in config, contradictory parameters")
        if category == "crawler":
            dictionary = self.crawler
            defaults = DEFAULTS_CRAWLER
        elif category == "database":
            dictionary = self.database
            defaults = DEFAULTS_DATABASE
        elif category == "purge":
            dictionary = self.purge
            defaults = DEFAULTS_PURGE
        elif category == "stats":
            dictionary = self.stats
            defaults = DEFAULTS_STATS
        elif category == "dump":
            dictionary = self.dump
            defaults = DEFAULTS_DUMP
        else:
            raise GeminiException("Internal error in config, unknown category %s" % category)
        if self.conf.get(category) is None or self.conf.get(category).get(param) is None:
            dictionary[param] = defaults[param]
        else:
            dictionary[param] = self.conf.get(category).get(param).params[0]
            if dictionary[param].lower() in ["none", "null", ""]:
                    dictionary[param] = None
            elif is_integer:
                try:
                    dictionary[param] = int(dictionary[param])
                except ValueError:
                    raise ConfigParsingException("Invalid value for integer: %s = %s" % (param, dictionary[param]))                    
            elif is_boolean:
                if dictionary[param].lower() in ["true", "1", "t", "y", "yes", "yeah", "yup", "certainly"]:
                    dictionary[param] = True
                elif dictionary[param].lower() in ["false", "0", "f", "n", "no", "no way"]:
                    dictionary[param] = False
                else:
                    raise ConfigParsingException("Invalid value for boolean: %s = %s" % (param, dictionary[param]))

    def __str__(self):
        return self.filename + " Crawler: " + str(self.crawler) + " Database: " + str(self.database)
    
if __name__ == "__main__":
    c = Config()
    print(c)
    
