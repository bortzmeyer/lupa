Find links *to* a given capsule:

SELECT DISTINCT(Urls_sources.url) FROM Origins,Urls AS Urls_sources,Urls AS Urls_destinations WHERE category = 'link'
AND Origins.destination = Urls_destinations.id AND Origins.source = Urls_sources.id AND Urls_destinations.capsule=(SELECT
id FROM Capsules WHERE name='CAPSULENAME');
